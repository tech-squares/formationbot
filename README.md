# FormationBot

FormationBot renders square dance formations as pictures, to aid discussions of calling. It is open source, under the MIT license.

## Formatting language

FormationBot supports a mini-language to indicate formations. A person is represented by a single character, with optional prefixes to modify rendering.

Facing directions are indicated with:
- `<`, `>`, `^`, and `v` (or `V`) represent a person facing in the appropriate direction
- `nsew` can be used similarly (north, south, etc.) when more convenient (for example, mobile keyboards)
- `,@*` all represent people with no facing direction
- `.` represents an empty location

Other common prefixes include:
- colors: `r`, `g`, `b`, and `y` for red, green, blue, and yellow, respectively
- labels: `x`, `o`, or a digit will fill the person box in appropriately
- phantoms: `p` will use a dashed box instead of a solid one

A newline or `/` will divide lines of the formation.

For complicated formations, one trick is that each line is centered, which often allows for a half-spot offset. For example, diamonds look like outer lines of three spots around an inner line of four, and the centering will offset them appropriately (`>.>/^V^V/<.<`). If that's not enough, you can also prefix `u`, `d`, `l`, or `R` (yes, case-sensitive) to shift a person up or down a half matrix spot.

For more details, the [renderer source](SquaresFormation/SquaresFormatter.py) is semi-readable. The bot renders to TeX using [`tex-squares.sty`](https://www.mit.edu/~tech-squares/resources/tex/), and then uses the [rtex web API](https://rtex.probablyaweb.site/docs) to get a PNG.

## Availability

The bot runs on several platforms:
- Tech Squares Zulip (trigger it by prefixing your message with `@FormationBot`)
- Hangouts (trigger it by prefixing your message with `/f` in a Hangout it's in)
- Discord (Tech Squares discord and PMs, but open adding it to other servers; triggered by prefixing your message with `/f`)

All known current instances are run by Alex.

### Discord

To run a Discord instance:
1. Create an integration
2. Create a bot
3. Create a token, and save it to discord.token (a simple file with just the token -- no quotes or similar)
4. Enable the message content intent
5. Under OAuth2, choose URL Generator
   1. For scopes, pick "bot" and "applications.command"
   2. For bot permissions, pick "Send Messages" and "Read Message History"
6. Visit the generated URL and add it to the relevant servers
