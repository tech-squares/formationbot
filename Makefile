all: check

check: pylint doctest
	
pylint:
	pylint --version
	pylint --rcfile=pylintrc.ini SquaresFormation

doctest:
	python3 -mdoctest SquaresFormation/SquaresFormatter.py
