#!/usr/bin/env python3
"""Discord frontend to FormationBot"""

import logging
import sys

import discord

import SquaresFormatter #pylint:disable=import-error

LOGGER = logging.getLogger(__name__)

class SquaresBot(discord.Bot):
    """Discord bot class"""
    def __init__(self, *args, **kwargs):
        """Create Discord bot"""
        super().__init__(*args, **kwargs)
        self.formatter = SquaresFormatter.SquaresFormatter()
        self.slash_command()(self.formation)

    async def on_ready(self):
        """Report readiness"""
        LOGGER.info('We have logged in as %s', self.user)

    async def on_message(self, message):
        """Handle message and possibly send formatted formation"""
        if message.author == self.user:
            return

        msg = message.content

        if "/f" in msg:
            # Reply to messages with formatted formation
            formation = msg.split("/f")[1]
            LOGGER.info("Received message '%s'", formation)
            rendered = self.formatter.render_formation(formation)
            uploaded = discord.File(rendered)
            LOGGER.info("Uploaded file: %s", uploaded)
            await message.reply(content="Formation:", file=uploaded, mention_author=False)

    @staticmethod
    async def formation(ctx, formation: str):
        """Render a squares formation"""
        self = ctx.bot

        if formation:
            LOGGER.info("Received slashcommand formation='%s'", formation)
            rendered = self.formatter.render_formation(formation)
            uploaded = discord.File(rendered)
            LOGGER.info("Uploaded file: %s", uploaded)
            await ctx.respond("Formation:", file=uploaded)
        else:
            LOGGER.info("Received slashcommand with formation")
            await ctx.respond("No formation found")

def main():
    """Create client and run bot"""
    intents = discord.Intents(messages=True, message_content=True)
    if sys.argv[1:]:
        bot = SquaresBot(intents=intents, debug_guilds=sys.argv[1:])
    else:
        bot = SquaresBot(intents=intents)
    @bot.command(description='latency')
    async def ping(ctx):
        await ctx.respond(f"Pong! Latency is {bot.latency}")
    with open('discord.token', 'r', encoding='utf-8') as token_fp:
        bot_password = token_fp.read().strip()
    bot.run(bot_password)


if __name__ == '__main__':
    logging.basicConfig()
    LOGGER.setLevel(logging.DEBUG)
    main()
