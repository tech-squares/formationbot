"""
Chatbot for generating formation diagrams
"""

import SquaresFormatter #pylint:disable=import-error

class SquaresFormation:
    '''
    A bot to format square dance formations
    '''

    def __init__(self):
        '''Constructor'''
        self.formatter = SquaresFormatter.SquaresFormatter()

    def usage(self):
        '''Return usage message'''
        usage = '''A bot to format square dance formations.

        Indicate dancers with facing direction with <, >, ^, or v.
        '''
        return usage

    def handle_message(self, message, bot_handler):
        '''Reply to messages with formatted formation'''
        formation = message['content']
        print("Received message '%s'" % formation)
        rendered = self.formatter.render_formation(formation)
        uploaded = bot_handler.upload_file(rendered)
        print(uploaded)
        print(uploaded['msg'])
        reply = "[%s](%s)" % ("formation", uploaded['uri'])
        bot_handler.send_reply(message, reply)

handler_class = SquaresFormation #pylint:disable=invalid-name
