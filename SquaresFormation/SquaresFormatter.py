#!/usr/bin/env python3
"""
Formatting backend for chatbot for generating formation diagrams
"""

import io
import os
import os.path
import string
import subprocess
import tempfile

import requests


FORMATION_NEWLINE = '\n\\cr\n'

DIRECTIONS = {
    '<': 'w',
    '>': 'e',
    '^': 'n',
    'v': 's',
    'V': 's',
    'w': 'w',
    'e': 'e',
    'n': 'n',
    's': 's',
    ',': 'x', # no facing dir
    '@': 'x', # no facing dir
    '*': 'x', # no facing dir
}

SHIFTS = {
    'u': 'n',
    'd': 's',
    'l': 'w',
    'R': 'e',
}

COLORS = {
    'r': 'red',
    'g': 'green',
    'b': 'blue',
    'y': 'yellow',
}

LABELS = set('xo'+string.digits)

# Make sure the various control characters are disjoint
def check_disjoint(*args):
    """Validate that several sets are disjoint

    >>> check_disjoint({'foo', 'bar'}, {'baz'})
    True

    >>> check_disjoint({'foo', 'bar'}, {'foo', 'quux'})
    False

    # Actually validate the control characters are disjoint
    >>> check_disjoint(DIRECTIONS.keys(), COLORS.keys(), SHIFTS.keys(), LABELS)
    True
    """

    combined = set()
    count = 0
    for arg in args:
        combined |= set(arg)
        count += len(arg)
    return len(combined) == count

class FormationConverter:
    """Converts a formation string to a LaTeX snippet"""

    def __init__(self):
        """Setup formation state"""
        self.snippets = []
        self.person_type = None # phantom/normal
        self.shift = None # shift a half spot in a direction
        self.color = None
        self.label = None
        self.dir = None

    def reset(self):
        """Reset dancer state"""
        self.person_type = None # phantom/normal
        self.shift = None # shift a half spot in a direction
        self.color = None
        self.label = None
        self.dir = None

    def flush(self, dancer):
        """Add a dancer to the output and reset state"""
        self.snippets.append(dancer)
        self.reset()

    def process(self, char):
        """Process another character of input"""

        # Modifiers
        if char in SHIFTS:
            self.shift = SHIFTS[char]
        elif char in COLORS:
            self.color = COLORS[char]
        elif char in LABELS:
            self.label = char
        elif char == 'p':
            self.person_type = 'p'
        # Issue #5 also suggests b/g as labels, but we prefer colors
        # Also circles don't seem to be supported by tex-squares.sty

        # Person indicators
        elif char in DIRECTIONS:
            dancer = '\\' + (self.shift or '') + (self.person_type or '') + 'dancer '
            dancer += (self.label or '.')
            dancer += DIRECTIONS[char]
            if self.color:
                dancer = r'\textcolor{%s}{%s}' % (self.color, dancer)
            self.flush(dancer)
        elif char == '.':
            self.flush(r'\idancer')
        elif char in ('\n', '/'):
            if self.snippets and self.snippets[-1] != FORMATION_NEWLINE:
                self.flush(FORMATION_NEWLINE)
        # Issue #5 also suggests "+" for a visible matrix spot ("+" icon),
        # but tex-squares.sty doesn't seem to support this

    def get_latex_snippet(self):
        """Return the output snippet"""
        return ''.join(self.snippets)

def convert_to_tex_snippet(snippet):
    r"""Turn a formation snippet into a TeX one

    >>> convert_to_tex_snippet("<>vV^")
    '\\dancer .w\\dancer .e\\dancer .s\\dancer .s\\dancer .n'
    >>> convert_to_tex_snippet("r@ pyx. p3bon dp*")
    '\\textcolor{red}{\\dancer .x}\\idancer\\textcolor{blue}{\\pdancer on}\\spdancer .x'
    """
    converter = FormationConverter()
    for char in snippet:
        converter.process(char)
    return converter.get_latex_snippet()


def render_tex_local(tex, out_format):
    """Render given LaTeX file contents into a PDF, locally

    Returns a path to a PDF. When things run as expected, should not leave
    detritus behind on the filesystem besides the PDF.
    """
    prefix = 'squares-formation.'
    name = None
    with tempfile.NamedTemporaryFile(mode='w', encoding='utf-8',
                                     prefix=prefix, suffix='.tex') as tex_fp:
        tex_fp.write(tex)
        tex_fp.flush()
        tempdir = os.path.dirname(tex_fp.name)
        try:
            cmd = ['pdflatex', '-halt-on-error', tex_fp.name]
            subprocess.check_call(cmd, cwd=tempdir)
        except subprocess.CalledProcessError:
            print(tex)
            raise
        name = tex_fp.name
    base, tex_ext = os.path.splitext(name)
    assert tex_ext == '.tex'
    pdf_name = base+'.pdf'
    assert os.path.exists(pdf_name)
    for ext in ['.aux', '.log']:
        os.remove(base+ext)
    if out_format == 'pdf':
        out_name = pdf_name
    elif out_format in ('png', 'gif'):
        out_name = base + '.' + out_format
        cmd = ['convert', '-alpha', 'deactivate', '-density', '500']
        cmd += [pdf_name, out_name]
        subprocess.check_call(cmd, cwd=tempdir)
        os.remove(pdf_name)
    else:
        raise ValueError("Unexpected output format %s" % (out_format, ))
    out_fp = open(out_name, 'rb') # pylint:disable=consider-using-with
    os.remove(out_name)
    return out_fp


RTEX_API = "http://rtex.probablyaweb.site/api/v2"

def render_tex_rtex(tex, out_format):
    """Render given LaTeX file contents, using rtex remote service"""
    if out_format not in ('pdf', 'png', 'jpg'):
        raise ValueError("Unexpected output format %s" % (out_format, ))
    compile_data = dict(code=tex, format=out_format)
    compile_resp = requests.post(RTEX_API, json=compile_data, timeout=10)
    compile_json = compile_resp.json()
    if compile_json['status'] != 'success':
        error = ("rtex failed to render document: %s" %
                 (compile_json.get('description', ''), ))
        raise ValueError(error)

    file_resp = requests.get(RTEX_API+'/'+compile_json['filename'], timeout=10)
    file_io = io.BytesIO(file_resp.content)
    file_io.name = compile_json['filename']
    return file_io



class SquaresFormatter:
    """Encapsulates methods for formatting squares formations"""

    def __init__(self):
        """
        Create SquaresFormatter, including reading templates from filesystem
        """
        with open('template.tex', 'r', encoding='utf-8') as template_fp:
            self.template = template_fp.read()
        with open('tex-squares.sty', 'r', encoding='utf-8') as sty_fp:
            self.squares_sty = sty_fp.read()


    def substitute_tex(self, formation):
        """
        Turn a formation snippet into a full file

        This takes the snippet, and embeds it into the LaTeX template
        (substituting the .sty contents as well to make something
        freestanding).
        """
        replaces = dict(
            TEXSQUARES=self.squares_sty,
            FORMATION=formation
        )
        out = self.template
        for key, value in replaces.items():
            out = out.replace("__"+key+"__", value)
        return out


    def render_formation(self, formation, out_format='png', local=False):
        """Turn a simple formation string into rendered contents"""
        snippet = convert_to_tex_snippet(formation)
        tex = self.substitute_tex(snippet)
        if local:
            out = render_tex_local(tex, out_format=out_format)
        else:
            out = render_tex_rtex(tex, out_format=out_format)
        return out
