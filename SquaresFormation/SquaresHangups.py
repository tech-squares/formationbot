"""
Google Hangouts bindings for chatbot for generating formation diagrams
"""

import asyncio
import sys
import hangups
import SquaresFormatter #pylint:disable=import-error


class SquaresFormation:
    """Hangouts/Hangups client for FormationBot"""
    def __init__(self, token_path):
        """Starts the bot.

        Args:
            token_path: The path to the authorization token.
        """

        self.trigger = "/f"
        # to avoid triggering on links
        self.end_trigger = "://"
        self.formatter = SquaresFormatter.SquaresFormatter()

        # These are populated by on_connect when it's called.
        self.conv_list = None
        self.user_list = None

        try:
            cookies = hangups.auth.get_auth_stdin(token_path)
        except hangups.GoogleAuthError as exc:
            sys.exit(f"Login failed ({exc})")

        self.client = hangups.Client(cookies)
        self.client.on_connect.add_observer(self.on_connect)

        self.loop = asyncio.new_event_loop()

        self.start_loop()

    def start_loop(self) -> None:
        """Start main loop."""
        asyncio.set_event_loop(self.loop)
        try:
            self.loop.run_until_complete(self.client.connect())
        except KeyboardInterrupt:
            self.loop.run_until_complete(self.client.disconnect())
        finally:
            self.loop.close()

    async def on_connect(self) -> None:
        """Handle connecting for the first time."""
        self.user_list, self.conv_list = await hangups.build_user_conversation_list(
            self.client
        )
        self.conv_list.on_event.add_observer(self.on_event)
        print("Connected!")

    async def on_event(self, conv_event: hangups.ConversationEvent) -> None:
        '''Reply to messages with formatted formation'''
        if not isinstance(conv_event, hangups.ChatMessageEvent):
            return
        if self.conv_list is None:
            return
        conv = self.conv_list.get(conv_event.conversation_id)
        user = conv.get_user(conv_event.user_id)
        if user.is_self or conv.is_quiet:
            return
        text = self.get_stripped_message(conv_event.text, len(conv.users) == 2)
        if not text:
            return
        conv_name = get_conv_name(conv)
        user_name = get_user_name(user, conv)
        if len(conv.users) == 2:
            print(f"Message from {user_name}:")
        else:
            print(f"Message from {user_name} to conversation {conv_name}:")
        print(text)

        print("Received message '%s'" % text)
        rendered = self.formatter.render_formation(text)
        await conv.send_message([], rendered)

    def get_stripped_message(self, text: str, direct: bool) -> str:
        """Returns the text to be processed by the bot.

        This includes all text from the first instance of the trigger to the
        first instance of the end trigger (or end of text, if the end trigger
        is not present). If the trigger is not present, the behavior depends on
        the `direct` flag:
        - `direct` is true: this is a direct message, so include all text from
          the start of the message
        - `direct` is false: this is not a direct message, so return the empty
          string to ignore the message

        If the end trigger occurs before the trigger, the empty string is
        returned.
        """
        start = text.find(self.trigger)
        if start < 0 and direct:
            start = 0
        elif start < 0:
            return ""
        else:
            start += len(self.trigger)
        end = text.find(self.end_trigger)
        if end < 0:
            return text[start:]
        else:
            return text[start:end]


def get_conv_name(
        conv: hangups.conversation.Conversation,
        truncate: bool = False,
        show_unread: bool = False,
) -> str:
    """Return a readable name for a conversation.

    If the conversation has a custom name, use the custom name. Otherwise, for
    one-to-one conversations, the name is the full name of the other user. For
    group conversations, the name is a comma-separated list of first names. If
    the group conversation is empty, the name is "Empty Conversation".

    If truncate is true, only show up to two names in a group conversation.

    If show_unread is True, if there are unread chat messages, show the number
    of unread chat messages in parentheses after the conversation name.
    """
    num_unread = len(
        [
            conv_event
            for conv_event in conv.unread_events
            if isinstance(conv_event, hangups.ChatMessageEvent)
            and not conv.get_user(conv_event.user_id).is_self
        ]
    )
    if show_unread and num_unread > 0:
        postfix = " ({})".format(num_unread)
    else:
        postfix = ""
    if conv.name is not None:
        return conv.name + postfix
    else:
        participants = sorted(
            (user for user in conv.users if not user.is_self), key=lambda user: user.id_
        )
        names = [user.first_name for user in participants]
        if len(participants) == 0:
            return "Empty Conversation" + postfix
        if len(participants) == 1:
            return participants[0].full_name + postfix
        elif truncate and len(participants) > 2:
            return ", ".join(names[:2] + ["+{}".format(len(names) - 2)]) + postfix
        else:
            return ", ".join(names) + postfix


def get_user_name(
        user: hangups.user.User,
        conv: hangups.conversation.Conversation,
) -> str:
    """Returns the first name of the user, unless multiple users in the
    conversation share a first name, in which case returns the full name."""
    if any(
            (
                u.first_name == user.first_name
                for u in conv.users
                if not u.is_self and u is not user
            )
    ):
        return user.full_name
    return user.first_name


if __name__ == "__main__":
    SquaresFormation("./token")
